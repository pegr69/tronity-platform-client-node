## tronity-platform-client

[![pipeline status](https://gitlab.com/tronity/tronity-platform-client-node/badges/master/pipeline.svg)](https://gitlab.com/tronity/tronity-platform-client-node/-/commits/master)
[![License](https://img.shields.io/badge/license-MIT-yellow.svg)](https://img.shields.io/badge/license-MIT-yellow.svg)

## Description

A NPM Package to easily access the Tronity Platform API.
Get data related to your vehicle like odometer, charge, battery
and location. Via the client you can conveniently get data from
the API and don't have to worry about recreating the token.

## Requirements

* nodeJS

## Installation

npm install tronity-platform --save

## Usage
```javascript
import * as tronity from 'tronity-platform';

// Get Access Token
const auth = await new tronity.AuthenticationApi().authControllerAuthentication({
    client_id: '',
    client_secret: '',
    grant_type: 'app'
});

// Get Vehicles
const vehicles = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).getManyBaseVehicleControllerVehicle();

// Get vin from the vehicle 
const vin = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).vehicleControllerVin(vehicles.data.data[0].id);

// Get odometer from the vehicle
const odometer = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).vehicleControllerOdometer(vehicles.data.data[0].id);

// Read battery's state of charge
const battery = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).vehicleControllerBattery(vehicles.data.data[0].id);

// Know whether vehicle is charging
const charging = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).vehicleControllerCharge(vehicles.data.data[0].id);

// Returns the last known location of the vehicle in geographic coordinates.
const location = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).vehicleControllerLocation(vehicles.data.data[0].id);

// Get avg consumption and distance for the month
const tripsAVG = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).tripControllerAvg(07, 2020, vehicles.data.data[0].id);

// Retrieve many Trip
const trips = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).getManyBaseTripControllerTrip(vehicles.data.data[0].id, undefined, undefined, ['createdAt,DESC'], undefined, 1, undefined);

// Retrieve many Charge
const charges = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).getManyBaseChargeControllerCharge(vehicles.data.data[0].id, undefined, undefined, ['createdAt,DESC'], undefined, 1, undefined);

// Retrieve many Sleep
const sleeps = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).getManyBaseSleepControllerSleep(vehicles.data.data[0].id, undefined, undefined, ['createdAt,DESC'], undefined, 1, undefined);

// Retrieve many Idle
const idles = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).getManyBaseIdleControllerIdle(vehicles.data.data[0].id, undefined, undefined, ['createdAt,DESC'], undefined, 1, undefined);

// Retrieve many Record
const records = await new tronity.VehiclesApi({
    accessToken: auth.data.access_token
}).getManyBaseRecordControllerRecord(vehicles.data.data[0].id, undefined, undefined, ['createdAt,DESC'], undefined, 1, undefined);
```